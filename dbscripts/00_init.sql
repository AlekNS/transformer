create table jobs (
  id uuid primary key,
  name varchar unique,
  status integer default 0,
  lock_expire integer default 0
);
