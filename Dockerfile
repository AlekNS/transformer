FROM node:12-alpine

ENV NPM_CONFIG_LOGLEVEL warn
WORKDIR /app

COPY . .
COPY .env.sample .env
RUN npm install && npm run build && rm -rf node_modules && npm install --production --no-optional && rm -rf src Dockerfile docker-compose.yml

USER node

CMD ["npm", "run", "main"]
