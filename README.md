Transformer
===========

Run
---

# `$ docker-compose up -d`

`fssrc` used as source and `fsdst` used as destination.

Copy any files to `fssrc`.

To scale `$ docker-compose scale transformer=10`.

Tests
-----

# `npm install`

# `npm run test:cover`
