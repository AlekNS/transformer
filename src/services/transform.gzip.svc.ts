import * as zlib from 'zlib';
import { Readable, Writable } from 'stream';
import { injectable } from 'inversify';

import config from '../config';
import { Transformable } from './interfaces';
import { Logger } from '../logger';

/**
 * Gzip Transformation
 */
@injectable()
class GzipTransform implements Transformable {
  private logger = Logger.get('GzipTransform');

  /**
   * @param reader
   * @param writer
   */
  async data(reader: Readable, writer: Writable) {
    return new Promise((resolve, reject) => {
      const gzip = zlib.createGzip({ memLevel: config.transform.gzip.level, level: config.transform.gzip.level });

      this.logger.debug('run transformation');

      reader.pipe(gzip)
        .on('error', (e) => {
          reject(e);
        })
        .pipe(writer)
        .on('error', (e) => {
          reject(e);
        })
        .on('finish', () => {
          resolve();
          this.logger.debug('transformation is done');
        });
    });
  }

  /**
   * @param name
   */
  readerName(name: string): string {
    return name;
  }

  /**
   * @param name
   */
  writerName(name: string): string {
    return name + '.gz';
  }
}

export default GzipTransform;
