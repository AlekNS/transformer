import { Readable, Writable } from 'stream';

type JobId = string;
type JobItem = string;
type MutexUnlock = () => Promise<void>;

interface Notification {
  on(event: string, callback: (msg) => Promise<void>);
  on(event: string, callback: (msg) => void);
  publish(channel: string, event: object): Promise<void>;
}

interface State {
  shutdown(): Promise<void>;

  notification(): Promise<Notification>;
  defineJobs(jobs: JobItem[]): Promise<void>;
  lockJob(item: JobItem): Promise<MutexUnlock>;
  markJobProcessed(item: JobItem): Promise<void>;
}

interface Source {
  readItems(): Promise<string[]>;
  getReader(item: string): Readable;
  remove(item: string): Promise<void>;
}

interface Destination {
  getWriter(item: string): Writable;
}

interface Transformable {
  readerName(name: string): string;
  writerName(name: string): string;

  data(reader: Readable, writer: Writable): Promise<any>;
}
