import { injectable } from 'inversify';
import * as util from 'util';
import * as stream from 'stream';
import * as fs from 'fs';
import * as path from 'path';

import config from '../config';
import { Source } from './interfaces';
import { Logger } from '../logger';

const readdir = util.promisify(fs.readdir);
const unlink = util.promisify(fs.unlink);

/**
 * Source items from folder
 */
@injectable()
class SourceFolder implements Source {
  private logger = Logger.get('SourceFolder');

  /**
   * Reads items from folder
   */
  async readItems(): Promise<string[]> {
    this.logger.addMeta({ path: config.source.path }).debug('read items');

    return readdir(config.source.path);
  }

  /**
   * Get reader for specified item
   * @param item
   */
  getReader(item: string): stream.Readable {
    const logger = this.logger.addMeta({ item });

    logger.debug('get reader');

    return fs.createReadStream(path.join(config.source.path, item)).on('close', () => {
      logger.debug('the reader is close');
    });
  }

  /**
   * @param item
   */
  async remove(item: string) {
    return unlink(path.join(config.source.path, item));
  }
}

export default SourceFolder;
