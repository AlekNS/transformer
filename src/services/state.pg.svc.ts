import { EventEmitter } from 'events';
import * as uuid from 'node-uuid';
import * as pg from 'pg';
import { injectable, decorate } from 'inversify';

import { Logger } from '../logger';
import { once, sleep } from '../helpers';
import config from '../config';

import { State, JobItem, MutexUnlock, JobId, Notification } from './interfaces';
import { ExceptionAlreadyLocked, ExceptionAlreadyProcessed } from './errors';
import { NOTIFICATION_EVENT, NOTIFICATION_CHANNEL, JobStatus } from './consts';

decorate(injectable(), EventEmitter);

const MIN_ATTEMPT_INTERVAL = 100;
const MAX_ATTEMPT_INTERVAL = 200;

/**
 * Notification pg implementation
 */
class NotificationPostgresService extends EventEmitter implements Notification {
  private logger = Logger.get('NotificationPostgresService');

  public constructor(private pgClient: pg.Client) { super(); }

  /**
   *
   */
  public async start() {
    this.pgClient.on(NOTIFICATION_EVENT, (msg: any) => {
      let result = msg.payload;
      try {
        result = JSON.parse(msg.payload);
        this.emit(NOTIFICATION_EVENT + '.' + msg.channel, {
          channel: msg.channel,
          type: 'message',
          payload: result,
        });
      } catch (err) {
        this.logger.error('failed to publish message to ' + msg.channel, { err });
      }
    });

    this.listen(NOTIFICATION_CHANNEL);
  }

  /**
   * @param payload
   */
  public async publish(channel: string, payload: object) {
    const client = await this.pgClient;
    await client.query(`NOTIFY ${channel}, '${JSON.stringify(payload)}'`);
  }

  /**
   *
   */
  public async shutdown() {
    this.logger.debug('clean all listeners');

    this.pgClient.removeAllListeners(NOTIFICATION_EVENT);
    this.eventNames().forEach(ev => this.removeAllListeners(ev));

    await this.unlisten(NOTIFICATION_CHANNEL);
  }

  private async listen(channel: string) {
    this.logger.debug('start listen channel ' + channel);

    const client = await this.pgClient;
    await client.query(`LISTEN ${channel}`);
  }

  private async unlisten(channel) {
    this.logger.debug('stop listen channel ' + channel);

    const client = await this.pgClient;
    await client.query(`UNLISTEN ${channel}`);
  }
}

/**
 * Application state
 */
@injectable()
export class StatePostgresService implements State {
  private logger = Logger.get('StatePostgresService');
  private pgNotification: NotificationPostgresService;
  private pgClient: pg.Client;
  private getClientMethod: any;

  /**
   *
   */
  public constructor() {
    this.pgClient = null;
    this.getClientMethod = once(async() => {
      this.logger.debug('establishing connection');

      const client = new pg.Client(config.state.url);

      await client.connect();
      this.pgClient = client;

      this.logger.debug('connection established');

      return true;
    }, true);

  }

  /**
   *
   */
  async client() {
    if (!this.pgClient) {
      await this.getClientMethod();
    }

    return this.pgClient;
  }

  async notification(): Promise<Notification> {
    if (!this.pgNotification) {
      this.pgNotification = new NotificationPostgresService(await this.client());
      await this.pgNotification.start();
    }

    return this.pgNotification;
  }

  /**
   *
   */
  async shutdown() {
    if (this.pgNotification) {
      await this.pgNotification.shutdown();
    }
    if (this.pgNotification) {
      await this.pgClient.end();
    }
  }

  /**
   * @param jobs
   */
  async defineJobs(jobs: JobItem[]) {
    this.logger.debug('define jobs');

    const chunkSize = config.defaultBatchSize;
    while (jobs.length > 0) {
      const chunk = jobs.splice(0, chunkSize);
      if (chunk.length === 0) {
        break;
      }

      await this.inTransaction(async() => {
        for (const job of chunk) {
          await this.pgClient.query(`
            INSERT INTO jobs (id, name, status) VALUES ($1, $2, $3) ON CONFLICT (name) DO NOTHING RETURNING id
          `, [uuid.v4(), job, JobStatus.pending]);
        }
      }, config.defaultAttempts);
    }
  }

  /**
   * @param item
   */
  private async doLock(item: JobItem) {
    const now = ~~(+new Date() / 1000);
    return this.inTransaction(async() => {
      const result = await this.pgClient.query(`
        SELECT id, status FROM jobs WHERE name = $1 AND lock_expire < $2 FOR UPDATE SKIP LOCKED
      `, [item, now]);
      if (!result.rowCount) {
        throw new ExceptionAlreadyLocked(item);
      }
      if (result.rows[0].status === JobStatus.processed) {
        throw new ExceptionAlreadyProcessed(item);
      }
      await this.pgClient.query(`
        UPDATE jobs SET lock_expire = $1 WHERE id = $2
      `, [now + ~~(config.lock.expireInterval / 1000), result.rows[0].id]);
    });
  }

  /**
   * @param item
   */
  async lockJob(item: JobItem): Promise<MutexUnlock> {
    await this.doLock(item);

    let refresherTimeoutHandler: NodeJS.Timeout;

    const refresherLockFn = () => {
      refresherTimeoutHandler = setTimeout(async() => {
        for (let i = 0; i < config.lock.retryAttempt; i++) {
          try {
            await this.doLock(item);
            refresherLockFn();
            break;
          } catch (err) {
            this.logger.error('fail to lock ' + item);
          }
        }
      }, (config.lock.expireInterval * 2) / 5);
    };

    refresherLockFn();

    return async() => {
      // nothing to unlock, simple remove time refresher.
      clearTimeout(refresherTimeoutHandler);
    };
  }

  /**
   * @param name
   */
  async markJobProcessed(name: JobItem) {
    await this.pgClient.query('UPDATE jobs SET status = $1 WHERE name = $2', [JobStatus.processed, name]);
    await this.pgNotification.publish(NOTIFICATION_CHANNEL, { item: name });
  }

  /**
   * @param callback
   */
  private async inTransaction(callback: () => Promise<void>, maxAttempts: number = 1) {
    for (let attempt = 0; attempt < maxAttempts; attempt++) {
      try {
        (await this.client()).query('BEGIN TRANSACTION ISOLATION LEVEL READ COMMITTED');
        await callback();
        await this.pgClient.query('COMMIT');
      } catch (err) {
        await this.pgClient.query('ROLLBACK');
        if (attempt === maxAttempts - 1) {
          throw err;
        }
        await sleep(Math.random() * (MAX_ATTEMPT_INTERVAL - MIN_ATTEMPT_INTERVAL) + MIN_ATTEMPT_INTERVAL);
      }
    }
  }
}
