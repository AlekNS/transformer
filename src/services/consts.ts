export enum JobStatus {
  pending = 0,
  processed = 1,
}
export const NOTIFICATION_EVENT = 'notification';
export const NOTIFICATION_CHANNEL = 'processed';
