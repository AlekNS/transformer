export const ConfigType = Symbol('Config');
export const DestinationType = Symbol('Destination');
export const SourceType = Symbol('Source');
export const TransformType = Symbol('Transform');
export const StateType = Symbol('State');
