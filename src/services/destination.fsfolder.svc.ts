import { injectable } from 'inversify';
import * as stream from 'stream';
import * as fs from 'fs';
import * as path from 'path';

import config from '../config';
import { Destination } from './interfaces';
import { Logger } from '../logger';

/**
 * Destination folder for items
 */
@injectable()
class DestinationFolder implements Destination {
  private logger = Logger.get('DestinationFolder');

  /**
   * Get writer for specified item
   * @param item
   */
  getWriter(item: string): stream.Writable {
    const logger = this.logger.addMeta({ item });

    logger.debug('get writer');

    return fs.createWriteStream(path.join(config.destination.path, item)).on('close', () => {
      logger.debug('the writer is close');
    });
  }
}

export default DestinationFolder;
