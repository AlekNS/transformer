import { Container, ContainerModule } from 'inversify';
import 'reflect-metadata';

import config from './config';
import JobTransformerApplication from './app/transform.app';
import { TransformAppType } from './app/types';
import { ConfigType, StateType, SourceType, DestinationType, TransformType } from './services/types';
import { Transformable, Destination, Source, State } from './services/interfaces';
import GzipTransform from './services/transform.gzip.svc';
import DestinationFolder from './services/destination.fsfolder.svc';
import SourceFolder from './services/source.fsfolder.svc';
import { StatePostgresService } from './services/state.pg.svc';

/* istanbul ignore next */
export function buildApplicationsContainerModule(): ContainerModule {
  return new ContainerModule((
    bind, unbind, isBound, rebind
  ) => {
    bind<JobTransformerApplication>(TransformAppType).to(JobTransformerApplication);
  });
}

/* istanbul ignore next */
export function buildServicesContainerModule(): ContainerModule {
  return new ContainerModule((
    bind, unbind, isBound, rebind
  ) => {
    bind<any>(ConfigType).toConstantValue(config);
    bind<State>(StateType).to(StatePostgresService);
    switch (config.destination.protocol) {
      case 'fsfolder:':
        bind<Source>(SourceType).to(SourceFolder);
        break;
      default:
        throw new Error(`${config.destination.protocol} is not supported for source`);
    }
    switch (config.destination.protocol) {
      case 'fsfolder:':
        bind<Destination>(DestinationType).to(DestinationFolder);
        break;
      default:
        throw new Error(`${config.destination.protocol} is not supported for destination`);
    }
    bind<Transformable>(TransformType).to(GzipTransform);
  });
}

/* istanbul ignore next */
export function buildIoc(): Container {
  const container = new Container();
  container.load(
    buildServicesContainerModule(),
    buildApplicationsContainerModule(),
  );
  return container;
}

export const container = buildIoc();
