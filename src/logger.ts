import * as winston from 'winston';

import config from './config';

export interface TaggedLogger {
  addMeta(meta: { [k: string]: any; }): TaggedLogger;
  debug(msg: string, ...meta: any[]);
  verbose(msg: string, ...meta: any[]);
  info(msg: string, ...meta: any[]);
  warn(msg: string, ...meta: any[]);
  error(msg: string, ...meta: any[]);
  critical(msg: string, ...meta: any[]);
}

export class Logger implements TaggedLogger {
  private static loggers: any = {};
  private logger: winston.Logger;

  /**
   * Get logger with name prefixed
   * @param name
   */
  static get(name: string): Logger {
    if (this.loggers[name]) {
      return this.loggers[name].addMeta({});
    }

    const outFormat = config.logging.format === 'json' ?
      winston.format.json({ space: config.logging.pretty ? 2 : 0 }) :
      winston.format.simple();

    this.loggers[name] = new Logger();
    this.loggers[name].logger = winston.createLogger({
      level: config.logging.level,
      format: winston.format.combine(winston.format.label({ label: name }), winston.format.timestamp(), outFormat),
      transports: [
        new winston.transports.Console({}),
      ],
    });

    return this.loggers[name];
  }

  debug(msg: string, ...meta: any[]) {
    return this.logger.debug(msg, ...meta);
  }

  verbose(msg: string, ...meta: any[]) {
    return this.logger.verbose(msg, ...meta);
  }

  info(msg: string, ...meta: any[]) {
    return this.logger.info(msg, ...meta);
  }

  warn(msg: string, ...meta: any[]) {
    return this.logger.warn(msg, ...meta);
  }

  error(msg: string, ...meta: any[]) {
    return this.logger.error(msg, ...meta);
  }

  critical(msg: string, ...meta: any[]) {
    return this.logger.crit(msg, ...meta);
  }

  addMeta(initialMeta): TaggedLogger {
    const meta = { ...initialMeta };
    return {
      addMeta: assignMeta => {
        return this.addMeta({ ...assignMeta, ...meta });
      },
      debug: (msg, ...strInterp) => {
        return this.debug(msg, ...strInterp, meta);
      },
      verbose: (msg, ...strInterp) => {
        return this.verbose(msg, ...strInterp, meta);
      },
      info: (msg, ...strInterp) => {
        return this.info(msg, ...strInterp, meta);
      },
      warn: (msg, ...strInterp) => {
        return this.warn(msg, ...strInterp, meta);
      },
      error: (msg, ...strInterp) => {
        return this.error(msg, ...strInterp, meta);
      },
      critical: (msg, ...strInterp) => {
        return this.critical(msg, ...strInterp, meta);
      },
    };
  }
}
