import * as chai from 'chai';

import { shuffle, once, sleep } from '../helpers';

const { expect } = chai;

describe('shuffle method', () => {
  describe('when call with empty array', () => {
    it('will success and returns empty array', () => {
      const result = shuffle([]);

      expect(result).has.length(0);
    });
  });

  describe('when call with 20 elements', () => {
    it('will success and returns 20 "shuffeled" elements', () => {
      const result = shuffle([...Array(20)].map((i, j) => j));

      let isConsec = true;
      for (let i = 0; i < 20; i++) {
        if (result[i] !== i) {
          isConsec = false;
          break;
        }
      }

      expect(result).has.length(20);
      expect(isConsec).is.false;
    });
  });
});

describe('once method', () => {
  describe('when call 2 times (lazy)', () => {
    it('will call callback only once', async() => {
      let counter = 0;
      const method = once(async() => { counter++; });

      expect(counter).is.eq(0);

      await method();
      await method();

      expect(counter).is.eq(1);
    });
  });

  describe('when call 2 times (eager)', () => {
    it('will call callback only once', async() => {
      let counter = 0;
      const method = once(async() => { counter++; }, false);

      await sleep(0);

      expect(counter).is.eq(1);

      await method();
      await method();

      expect(counter).is.eq(1);
    });
  });
});
