import { inject, injectable } from 'inversify';

import { Source, Destination, Transformable, JobItem, State, MutexUnlock } from '../services/interfaces';
import { shuffle, sleep } from '../helpers';
import { Logger } from '../logger';
import config from '../config';
import { SourceType, DestinationType, TransformType, StateType } from '../services/types';
import { ExceptionAlreadyProcessed } from '../services/errors';
import { NOTIFICATION_EVENT, NOTIFICATION_CHANNEL } from '../services/consts';

/**
 *
 */
@injectable()
export default class JobTransformerApplication {
  private logger = Logger.get('JobTransformerApplication');

  private pending = new Set<JobItem>();
  private repeat = new Set<JobItem>();
  private processed = new Set<JobItem>();

  /**
   * @param src
   * @param dst
   * @param transform
   * @param state
   */
  constructor(
    @inject(SourceType) private src: Source,
    @inject(DestinationType) private dst: Destination,
    @inject(TransformType) private transform: Transformable,
    @inject(StateType) private state: State) {
  }

  /**
   *
   */
  async run() {
    this.logger.debug('run');

    try {
      (await this.state.notification()).on(`${NOTIFICATION_EVENT}.${NOTIFICATION_CHANNEL}`, (msg) => {
        this.pending.delete(msg.payload.item);
        this.repeat.delete(msg.payload.item);
        this.processed.add(msg.payload.item);
      });

      while (true) {
        await this.preFillJobs();
        await this.processJobs();

        if (!config.sourceScanInterval) {
          break;
        }

        // @TODO: clean old sets (pending, repeat, processed)
        await sleep(config.sourceScanInterval);
      }

      this.logger.debug('done');
    } catch (err) {
      this.logger.error('error', { err });
    } finally {
      await this.state.shutdown();
    }
  }

  /**
   *
   */
  async preFillJobs() {
    this.logger.debug('get job items from source');

    const items = shuffle(await this.src.readItems());

    for (const item of items) {
      if (this.processed.has(item)) {
        continue;
      }
      this.pending.add(item);
    }

    this.logger.debug('store jobs state');

    await this.state.defineJobs(items);
  }

  /**
   *
   */
  async processJobs() {
    const logger = this.logger.addMeta({ method: 'processJobs' });

    logger.debug('process jobs');

    for (let jobItem of this.pending) {
      await this.processJobItem(jobItem);
    }

    // first attempt to check repeat job
    if (!this.repeat.size) {
      logger.debug('there no more jobs');
      return;
    }

    await sleep(config.repeatSleepInterval);

    // could be empty after sleep
    if (!this.repeat.size) {
      logger.debug('there no more jobs after sleep');
      return;
    }

    for (const jobItem of this.repeat.keys()) {
      this.pending.add(jobItem);
    }
    this.repeat.clear();

    logger.debug('repeat process jobs');

    return this.processJobs();
  }

  /**
   * @param item
   */
  async processJobItem(item: JobItem) {
    const logger = this.logger.addMeta({ item, method: 'processJobItem' });

    if (this.processed.has(item)) {
      logger.debug('skip job');
      return;
    }

    let mutexUnlock: MutexUnlock;

    logger.info('process job');

    try {
      mutexUnlock = await this.state.lockJob(item);
      await this.transformJobItem(item);
      await this.markProcessed(item);
      await this.src.remove(item);
    } catch (err) {
      if (err instanceof ExceptionAlreadyProcessed) {
        logger.debug('job is already processed, try to clean');
        try {
          await this.src.remove(item);
          this.processed.add(item);
        } catch (err) {
          logger.warn('failed to clean processed job item');
        }
      }
      this.pending.delete(item);
      if (!this.processed.has(item)) {
        logger.debug('add job for repeating');
        this.repeat.add(item);
      }
    } finally {
      if (mutexUnlock) {
        await mutexUnlock();
      }
    }
  }

  /**
   * @param item
   */
  async transformJobItem(item: JobItem) {
    const logger = this.logger.addMeta({ item, method: 'transformJobItem' });

    const reader = this.src.getReader(this.transform.readerName(item));
    const writer = this.dst.getWriter(this.transform.writerName(item));

    logger.debug('call transform data');

    await this.transform.data(reader, writer);
  }

  /**
   * @param item
   */
  async markProcessed(item: JobItem) {
    const logger = this.logger.addMeta({ item, method: 'markProcessed' });

    logger.debug('call state');

    await this.state.markJobProcessed(item);
    this.pending.delete(item);
    this.processed.add(item);
  }
}
