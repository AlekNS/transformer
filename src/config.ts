import 'reflect-metadata';
import * as url from 'url';
import * as dotenv from 'dotenv';

dotenv.config();

const {
  LOGGING_PRETTY,
  LOGGING_LEVEL,
  LOGGING_FORMAT,
  LOGGING_COLORIZE,

  GZIP_LEVEL,

  STATE_URL,
  SOURCE_URL,
  DESTINATION_URL,

  REPEAT_SLEEP,

  SOURCE_SCAN_INTERVAL,
} = process.env;

if (!STATE_URL) {
  throw new Error('STATE_URL is not specified');
}

if (!SOURCE_URL) {
  throw new Error('SOURCE_URL is not specified');
}

if (!DESTINATION_URL) {
  throw new Error('DESTINATION_URL is not specified');
}

export default {
  logging: {
    pretty: LOGGING_PRETTY === 'true',
    level: LOGGING_LEVEL || 'info',
    format: LOGGING_FORMAT,
    colorize: LOGGING_COLORIZE === 'true'
  },
  lock: {
    expireInterval: 20000,
    retryAttempt: 2,
  },
  defaultAttempts: 5,
  defaultBatchSize: 32,
  repeatSleepInterval: parseInt(REPEAT_SLEEP || '5000', 10),
  transform: {
    gzip: {
      level: parseInt(GZIP_LEVEL || '5', 10),
    },
  },
  state: {
    url: STATE_URL,
  },
  source: url.parse(SOURCE_URL, true),
  sourceScanInterval: parseInt(SOURCE_SCAN_INTERVAL || '0', 10),
  destination: url.parse(DESTINATION_URL, true),
};
