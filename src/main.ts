import * as util from 'util';

import { Logger } from './logger';
import { container } from './ioc.container';
import { TransformAppType } from './app/types';
import JobTransformerApplication from './app/transform.app';

const logger = Logger.get('ROOT');

process.on('uncaughtException', (err: any) => {
  logger.error(
    `Stop process. Uncaught Exception. Reason: ${util.inspect(err)}`
  );
  process.exit(1);
});

process.on('unhandledRejection', (reason: any, prom: any) => {
  logger.error(
    `Stop process. Unhandled Promise Rejection. Reason: ${util.inspect(reason)}, Promise: ${util.inspect(prom)}`
  );
  process.exit(1);
});

async function main() {
  logger.info('Run application');
  await container.get<JobTransformerApplication>(TransformAppType).run();
  logger.info('Stop application');
}

main();
