/**
 *
 * @param promiseMethod
 * @param lazy
 */
export function once<T>(promiseMethod: () => Promise<T>, lazy: boolean = true) {
  let promise = lazy ? null : promiseMethod();
  return async() => {
    if (promise !== null) {
      return promise;
    }
    promise = promiseMethod();
    return promise;
  };
}

/**
 * @param a
 */
export function shuffle<T>(a: Array<T>) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

/**
 * @param interval
 */
export function sleep(interval: number): Promise<void> {
  return new Promise((resolve, _) => {
    setTimeout(resolve, interval);
  });
}
